# translation of katekonsoleplugin.po to Punjabi
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# A S Alam <aalam@users.sf.net>, 2008, 2009, 2010.
msgid ""
msgstr ""
"Project-Id-Version: katekonsoleplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-13 01:39+0000\n"
"PO-Revision-Date: 2010-01-31 12:21+0530\n"
"Last-Translator: A S Alam <aalam@users.sf.net>\n"
"Language-Team: ਪੰਜਾਬੀ <punjabi-users@lists.sf.net>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: kateconsole.cpp:56
#, kde-format
msgid "You do not have enough karma to access a shell or terminal emulation"
msgstr "ਤੁਹਾਨੂੰ ਇੱਕ ਸ਼ੈੱਲ ਜਾਂ ਟਰਮੀਨਲ ਏਮੂਲੇਸ਼ਨ ਅਸੈੱਸ ਕਰਨ ਲਈ ਲੋੜੀਦੇ ਅਧਿਕਾਰ ਨਹੀਂ ਹਨ।"

#: kateconsole.cpp:104 kateconsole.cpp:134 kateconsole.cpp:653
#, kde-format
msgid "Terminal"
msgstr "ਟਰਮੀਨਲ"

#: kateconsole.cpp:143
#, kde-format
msgctxt "@action"
msgid "&Pipe to Terminal"
msgstr "ਟਰਮੀਨਲ ਲਈ ਪਾਇਪ(&P)"

#: kateconsole.cpp:147
#, kde-format
msgctxt "@action"
msgid "S&ynchronize Terminal with Current Document"
msgstr "ਮੌਜੂਦਾ ਡੌਕੂਮੈਂਟ ਨਾਲ ਟਰਮੀਨਲ ਸੈਕਰੋਨਾਇਜ਼ ਕਰੋ(&y)"

#: kateconsole.cpp:151
#, kde-format
msgctxt "@action"
msgid "Run Current Document"
msgstr ""

#: kateconsole.cpp:156 kateconsole.cpp:503
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "&Focus Terminal"
msgctxt "@action"
msgid "S&how Terminal Panel"
msgstr "ਟਰਮੀਨਲ ਫੋਕਸ(&F)"

#: kateconsole.cpp:162
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "&Focus Terminal"
msgctxt "@action"
msgid "&Focus Terminal Panel"
msgstr "ਟਰਮੀਨਲ ਫੋਕਸ(&F)"

#: kateconsole.cpp:302
#, kde-format
msgid ""
"Konsole not installed. Please install konsole to be able to use the terminal."
msgstr ""

#: kateconsole.cpp:378
#, kde-format
msgid ""
"Do you really want to pipe the text to the console? This will execute any "
"contained commands with your user rights."
msgstr ""

#: kateconsole.cpp:379
#, kde-format
msgid "Pipe to Terminal?"
msgstr "ਟਰਮੀਨਲ ਲਈ ਪਾਇਪ?"

#: kateconsole.cpp:380
#, kde-format
msgid "Pipe to Terminal"
msgstr "ਟਰਮੀਨਲ ਲਈ ਪਾਇਪ"

#: kateconsole.cpp:408
#, fuzzy, kde-format
#| msgid "Sorry, can not cd into '%1'"
msgid "Sorry, cannot cd into '%1'"
msgstr "ਅਫਸੋਸ, ਪਰ '%1' ਵਿੱਚ cd ਨਹੀਂ ਹੋ ਸਕਦਾ"

#: kateconsole.cpp:444
#, kde-format
msgid "Not a local file: '%1'"
msgstr ""

#: kateconsole.cpp:477
#, kde-format
msgid ""
"Do you really want to Run the document ?\n"
"This will execute the following command,\n"
"with your user rights, in the terminal:\n"
"'%1'"
msgstr ""

#: kateconsole.cpp:484
#, fuzzy, kde-format
#| msgid "Pipe to Terminal?"
msgid "Run in Terminal?"
msgstr "ਟਰਮੀਨਲ ਲਈ ਪਾਇਪ?"

#: kateconsole.cpp:485
#, kde-format
msgid "Run"
msgstr ""

#: kateconsole.cpp:500
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "&Pipe to Terminal"
msgctxt "@action"
msgid "&Hide Terminal Panel"
msgstr "ਟਰਮੀਨਲ ਲਈ ਪਾਇਪ(&P)"

#: kateconsole.cpp:511
#, fuzzy, kde-format
#| msgid "Defocus Terminal"
msgid "Defocus Terminal Panel"
msgstr "ਟਰਮੀਨਲ ਡਿ-ਫੋਕਸ"

#: kateconsole.cpp:512 kateconsole.cpp:513
#, fuzzy, kde-format
#| msgid "Focus Terminal"
msgid "Focus Terminal Panel"
msgstr "ਟਰਮੀਨਲ ਫੋਕਸ"

#: kateconsole.cpp:586
#, kde-format
msgid ""
"&Automatically synchronize the terminal with the current document when "
"possible"
msgstr "ਜਦੋਂ ਵੀ ਸੰਭਵ ਹੋਵੇ ਤਾਂ ਮੌਜੂਦਾ ਡੌਕੂਮੈਂਟ ਨਾਲ ਟਰਮੀਨਲ ਆਟੋਮੈਟਿਕ ਹੀ ਸੈਕਰੋਨਾਇਜ਼ ਕਰੋ(&A)"

#: kateconsole.cpp:590 kateconsole.cpp:611
#, fuzzy, kde-format
#| msgid "Pipe to Terminal"
msgid "Run in terminal"
msgstr "ਟਰਮੀਨਲ ਲਈ ਪਾਇਪ"

#: kateconsole.cpp:592
#, kde-format
msgid "&Remove extension"
msgstr ""

#: kateconsole.cpp:597
#, kde-format
msgid "Prefix:"
msgstr ""

#: kateconsole.cpp:605
#, kde-format
msgid "&Show warning next time"
msgstr ""

#: kateconsole.cpp:607
#, kde-format
msgid ""
"The next time '%1' is executed, make sure a warning window will pop up, "
"displaying the command to be sent to terminal, for review."
msgstr ""

#: kateconsole.cpp:618
#, kde-format
msgid "Set &EDITOR environment variable to 'kate -b'"
msgstr ""

#: kateconsole.cpp:621
#, kde-format
msgid ""
"Important: The document has to be closed to make the console application "
"continue"
msgstr ""

#: kateconsole.cpp:624
#, kde-format
msgid "Hide Konsole on pressing 'Esc'"
msgstr ""

#: kateconsole.cpp:627
#, kde-format
msgid ""
"This may cause issues with terminal apps that use Esc key, for e.g., vim. "
"Add these apps in the input below (Comma separated list)"
msgstr ""

#: kateconsole.cpp:658
#, kde-format
msgid "Terminal Settings"
msgstr "ਟਰਮੀਨਲ ਸੈਟਿੰਗ"

#. i18n: ectx: Menu (tools)
#: ui.rc:6
#, kde-format
msgid "&Tools"
msgstr "ਟੂਲ(&T)"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "aalam@users.sf.net"

#, fuzzy
#~| msgid "Terminal"
#~ msgid "Kate Terminal"
#~ msgstr "ਟਰਮੀਨਲ"

#, fuzzy
#~| msgid "Terminal"
#~ msgid "Terminal Panel"
#~ msgstr "ਟਰਮੀਨਲ"

#~ msgid "Konsole"
#~ msgstr "ਕਨਸੋਲ"

#~ msgid "Embedded Konsole"
#~ msgstr "ਇੰਬੈੱਡ ਕੀਤੀ ਕਨਸੋਲ"
